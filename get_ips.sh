#!/bin/bash

# Exit if any of the intermediate steps fail
set -e

# Extract input vars into env vars: api_key, iae_api_url
eval "$(jq -r '@sh "API_KEY=\(.api_key) IAE_API_URL=\(.iae_api_url)"')"

#echo "API_KEY=$API_KEY" >> debug.$$
#echo "IAE_API_URL=$IAE_API_URL" >> debug.$$

if [ x"$IAE_API_URL" = "x" -o x"$IAE_API_URL" = "xnull" ]; then
#  echo "No url passed" >> debug.$$
  echo '{ "public_ips": "" }'
else
#  echo "IAE_API_URL is set" >> debug.$$

  # Get an access token
  export RAW_ACCESS_TOKEN=`curl -k -X POST \
    --header "Content-Type: application/x-www-form-urlencoded" \
    --header "Accept: application/json" \
    --data-urlencode "grant_type=urn:ibm:params:oauth:grant-type:apikey" \
    --data-urlencode "apikey=$API_KEY" \
    "https://iam.cloud.ibm.com/identity/token" | jq '.access_token' `
  export ACCESS_TOKEN=`echo $RAW_ACCESS_TOKEN| sed 's/"//g'`

# Query IAE and format the nodes' public ip addresses from the answer into json
  curl --request GET --url $IAE_API_URL --header "authorization: Bearer $ACCESS_TOKEN" --header 'content-type : application/json' | jq '{ "public_ips": [.nodes |  unique_by(.public_ip)[] | .public_ip] | join(":")}'
  #cat curl_result | jq '{ "public_ips": [.nodes |  unique_by(.public_ip)[] | .public_ip] | join(":")}' >> debug.$$
fi

