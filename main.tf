##############################################################################
# Provider
##############################################################################
provider ibm {
  ibmcloud_api_key = var.ibmcloud_api_key
  region           = var.ibm_region
  generation       = var.generation
  ibmcloud_timeout = 60
}

##############################################################################
# Data
##############################################################################
data ibm_resource_group group {
  name = var.resource_group
}

data ibm_is_vpc vpc {
  name = var.vpc_name
}

# Retrieve the list of IAE public IP addresses from IAE API
data external iae_ip_addresses {
  program = ["sh", "${path.module}/get_ips.sh"]
  query = {
    "api_key" = var.ibmcloud_api_key
    "iae_api_url" = var.iae_api_url
  }
}

##############################################################################
# ACL
##############################################################################
resource ibm_is_network_acl metrics_server_acl {
  for_each = var.iae_api_url == null ? toset([]) : toset(["dummy"]) // Create this resource if and only if iae_api_url not null
  name = var.acl_name
  vpc = data.ibm_is_vpc.vpc.id 
  resource_group = data.ibm_resource_group.group.id
 
  rules {
    name        = "allow-all-outbound"
    action      = "allow"
    destination = "0.0.0.0/0"
    direction   = "outbound"
    source      = "0.0.0.0/0"
  }

  dynamic rules {

    for_each = toset(split(":", data.external.iae_ip_addresses.result["public_ips"]))
    content {
      name        = join("-", ["iae-inbound", replace(rules.key, ".", "-")])
      action      = "allow"
      source      = rules.key
      destination = "0.0.0.0/0"
      direction   = "inbound"
      tcp {
        port_max        = var.port_for_iae
        port_min        = var.port_for_iae
      }
    }
  }
}
