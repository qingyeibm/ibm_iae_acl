# IAE ACL

This module creates the following infrastructre

    1 x subnet ACL

This module queries IAE for a list of its nodes and deterimines the public IP address of each node.
The ACL created contains 
- inbound allow rules from (source: any of the IAE IP addresses, any port) to (destination: any address, port configured in variables)
- an outbound allow rule from (source: any IP, any port) to (destiniation: any IP, any port)

