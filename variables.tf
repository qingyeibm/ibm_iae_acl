##############################################################################
#  Account variables
##############################################################################
variable ibmcloud_api_key {
   description = "The IBM Cloud platform API key needed to deploy IAM enabled resources"
   type        = string
   default     = ""
}

variable generation {
   description = "Generation of VPC - 1 or 2"
   type        = number
   default     = 1
}

variable ibm_region {
   description = "IBM Cloud region where all resources will be deployed"
   type        = string
   default     = "eu-de"
}

variable resource_group {
   description = "Name for IBM Cloud Resource Group where resources will be deployed"
   type        = string
   default     = "truata"
}

##############################################################################
#  IAE location
##############################################################################
variable iae_api_url {
    description = "IAE API URL. Take this from the IAE service credentials page. e.g. https://api.eu-de.ae.cloud.ibm.com/v2/analytics_engines/eeeee111-c666-6666-aa66-eeeee666666"
    type        = string
    default     = null
} 

##############################################################################
#  Port to expose to IAE
##############################################################################
variable port_for_iae {
    description = "Which port we want to open to IAE nodes"
    type       = number
    default     = 2003
}

##############################################################################
#  VPC/VSI Variables, etc.
##############################################################################
variable vpc_name {
    description = "Name of VPC where ACL will be used"
    type        = string
    default     = "trua-g1-vpc"
}

variable zone {
    description = "Name of zone"
    type        = string
    default     = "eu-de-3"
}

variable acl_name {
    description = "Subnet ACL"
    type        = string
    default     = "iae-inbound-acl"
}
